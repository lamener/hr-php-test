<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'IndexController@weather')->name('weather');
Route::get('/weather', 'IndexController@weather')->name('weather');
Route::prefix('orders')->group(function () {
    Route::get('/', 'OrdersController@index')->name('orders.list');
    Route::get('/edit/{order}', 'OrdersController@edit')->name('order.edit');
//    Route::get('/{order}', 'OrdersController@show')->name('order.show');
    Route::post('/', 'OrdersController@store')->name('order.store');
});
Route::prefix('products')->group(function () {
    Route::get('/', 'ProductsController@index')->name('product.list');
});
