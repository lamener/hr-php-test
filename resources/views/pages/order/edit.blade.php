@extends('layouts.app')
@section('title', 'Список заказов')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    {!! Form::open(['url' => route('order.store')]) !!}
    {!! Form::hidden('id', $order->id) !!}
    <div class="form-group {{ $errors->has('client_email') ? 'has-error' : '' }}">
        {!! Form::label('client_email', 'Почта клиента:') !!}
        {!! Form::text('client_email', $order->client_email, ['class' => 'form-control']) !!}
        <span class="text-danger">{{ $errors->first('client_email') }}</span>
    </div>
    <div class="form-group">
        {!! Form::label('partner', 'партнер:') !!}
        {!! Form::select('partners', $partners, $order->partner_id, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Состав заказа:</label>
        <ul>
            @foreach($order->orderProducts as $product)
                <li>{{$product->product->name}} количество {{$product->quantity}} шт. стоимость {{$product->price}}</li>
            @endforeach
        </ul>
    </div>
    <div class="form-group">
        {!! Form::label('status', 'статус:') !!}
        {!! Form::select('statuses', $statuses, $status, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label>Стоимость заказа: </label>
        {{$cost}}
    </div>
    <div class="form-group">
        {!! Form::submit('Сохранить',['class' => 'btn btn-primary']) !!}
        <a class="btn btn-info" href="{{route('orders.list')}}">Вернуться к списку Заказов</a>
    </div>
    {!! Form::close() !!}
@stop