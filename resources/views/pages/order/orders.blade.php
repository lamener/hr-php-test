@extends('layouts.app')
@section('title', 'Список заказов')
@section('content')
    {{ $ordersList->links() }}
    @include('pages.order.partials.list', ['ordersList' => $ordersList])
    {{ $ordersList->links() }}
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li><a href="#past_due" data-toggle="tab">просроченные</a></li>
        <li><a href="#current" data-toggle="tab">текущие</a></li>
        <li><a href="#new" data-toggle="tab">новые</a></li>
        <li><a href="#completed" data-toggle="tab">выполненные</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="past_due">
                @include('pages.order.partials.list', ['ordersList' => $pastDueOrders])
        </div>
        <div class="tab-pane" id="current">
            @include('pages.order.partials.list', ['ordersList' => $currentOrders])
        </div>
        <div class="tab-pane" id="new">
            @include('pages.order.partials.list', ['ordersList' => $newOrders])
        </div>
        <div class="tab-pane" id="completed">
            @include('pages.order.partials.list', ['ordersList' => $completedOrders])
        </div>
    </div>

@endsection