<div class="orders-list">
    <div class="orders-list--header">
        <div class="col-xs-2 orders-list--header--col">
            Номер заказа
        </div>
        <div class="col-xs-2 orders-list--header--col">
            название партнера
        </div>
        <div class="col-xs-1 orders-list--header--col">
            стоимость заказа
        </div>
        <div class="col-xs-4 orders-list--header--col">
            наименование состав заказа
        </div>
        <div class="col-xs-1 orders-list--header--col">
            статус заказа
        </div>
        <div class="col-xs-2 orders-list--header--col">
            Дата доставки
        </div>
    </div>
    @foreach($ordersList as $order)
        <div class="orders-list--row">
            <div class="col-xs-2 orders-list--row--col">
                <a class="btn btn-info btn-xs" href="{{route('order.edit',['order' => $order->id])}}" target="_blank">Заказ №{{$order->id}}</a>
            </div>
            <div class="col-xs-2 orders-list--row--col">
                {{$order->partner->name}}
            </div>
            <div class="col-xs-1 orders-list--row--col">
                {{$order->total}}
            </div>
            <div class="col-xs-4 orders-list--row--col">
                @foreach($order->orderProducts as $product)
                    <div>{{$product->product->name}} кол-во = {{$product->quantity}} стоимость = {{$product->price}}
                        итого={{$product->quantity*$product->price}}</div>
                @endforeach
            </div>
            <div class="col-xs-1 status orders-list--row--col">
                {{\App\Enums\OrderStatuses::getTranslate($order->status)}}
            </div>
            <div class="col-xs-2 orders-list--row--col">
                {{$order->delivery_dt->format('H:i d-m-Y')}}
            </div>
        </div>
    @endforeach
</div>