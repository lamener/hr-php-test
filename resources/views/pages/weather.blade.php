@extends('layouts.app')
@section('title', 'Погода в Яндекс API')
@section('content')
    @if($error)
        {{$error}}
    @else
        <p>
            <img width="40" src="https://yastatic.net/weather/i/icons/blueye/color/svg/{{$temperature['fact']['icon']}}.svg" alt="">
            Текущая температура в Брянске {{$temperature['fact']['temp']>0?'+':'-'}} {{$temperature['fact']['temp']}} °C</p>
        <p>
            <a href="{{$temperature['info']['url']}}">Подробнее</a></p>
    @endif
@endsection
