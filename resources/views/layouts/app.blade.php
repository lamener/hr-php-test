<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{mix('css/style.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('js/app.js')}}" defer></script>
    <title>Тестовое приложение - @yield('title')</title>
</head>
<body>
@section('header')
    <nav class="navbar navbar-fixed-top navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('weather')}}">Погода</a></li>
                    <li class="active"><a href="{{route('orders.list')}}">Заказы</a></li>
                    <li><a href="{{route('product.list')}}">Продукты</a></li>
                </ul>
            </div><!-- /.nav-collapse -->
        </div><!-- /.container -->
    </nav>
@show
<div class="container">
    @yield('content')
</div>
@section('footer')
    <footer class="footer">
        <hr>
        <div class="container">
            <p class="text-muted">Place sticky footer content here.</p>
        </div>
    </footer>
@show
</body>
</html>