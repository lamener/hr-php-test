<?php
return [
    'statuses' => [
        'new' => 'Новый',
        'confirm' => 'Подтвержден',
        'completed' => 'Завершен'
    ],
    'save-statuses' =>[
        'success' => "Заказ успешно сохранен."
    ]
];