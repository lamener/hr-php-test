<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrdersControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->call('GET', 'orders');
        $response->assertViewHas('ordersList');
    }

    public function testEdit(){
        $response = $this->call('GET', 'orders/edit/1');
        $response->assertStatus(200);
    }
}
