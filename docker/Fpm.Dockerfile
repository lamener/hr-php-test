FROM php:7.1-fpm as scratch

# Copy composer.lock and composer.json
COPY composer.json /var/www/
# Set working directory
WORKDIR /var/www

COPY . .
# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6 \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    nodejs npm
# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN npm cache clean -f
RUN npm install npm -g

# Install extensions
#RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-install pdo_mysql zip exif pcntl
# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www
# Copy existing application directory contents
COPY . /var/www
# Copy existing application directory permissions
COPY --chown=www:www . /var/www
RUN chmod -R 777 /var/www
# Change current user to www
USER www
FROM scratch as local
RUN cp -rf .env.local .env

FROM scratch as development
RUN cp -rf .env.development .env
#RUN php artisan storage:link

CMD ["php-fpm"]

#RUN composer install --no-interaction --optimize-autoloader


