<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Weather\YandexWeather;

class WeatherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('weather.service', function ($app) {
            return new YandexWeather();
        });
    }
}
