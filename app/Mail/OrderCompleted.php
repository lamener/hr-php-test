<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $cost;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     * @param $cost
     */
    public function __construct(Order $order, $cost)
    {
        $this->order = $order;
        $this->cost = $cost;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.orders.completed')->subject("Заказ №{$this->order->id} завершен");
    }
}
