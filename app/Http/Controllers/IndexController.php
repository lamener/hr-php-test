<?php

namespace App\Http\Controllers;

use App\Services\Weather\WeatherInterface;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function weather()
    {
        /** @var WeatherInterface $weatherService */
        $weatherService = resolve('weather.service');
        $temperature = $weatherService->get(53.2520900, 34.3716700);
        $error = isset($temperature['error']) ? $temperature['error'] : '';
        return view('pages.weather', compact('temperature', 'error'));
    }
}
