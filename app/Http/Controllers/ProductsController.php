<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller
{
    const PER_PAGE = 25;

    public function index()
    {
//        $productsList = Product::paginate(self::PER_PAGE);
//        return View::make('pages.product.list')->with('productsList', $productsList);
        return view('pages.product.list');
    }

    public function indexApi()
    {
        $productsList = Product::with('vendor')->paginate(self::PER_PAGE);
        foreach ($productsList as $product) {
            $product->show = false;
        }
        return response()->json([
            'products' => $productsList
        ]);
    }

    public function saveNewPrice(Request $request)
    {
        $product = Product::where('id', $request->get('product_id'))
            ->update([
                'price' => $request->get('price')
            ]);

        if (!$product) {
            return response()->json([
                'error' => trans('products.save-statuses.error')
            ]);
        }

        return response()->json([
            'message' => trans('products.save-statuses.success')
        ]);
    }
}
