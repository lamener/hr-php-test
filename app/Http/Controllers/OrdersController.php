<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatuses;
use App\Managers\OrderManager;
use App\Models\Order;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use ReflectionClass;

class OrdersController extends Controller
{
    private $manager;
    const PER_PAGE = 20;

    public function __construct(OrderManager $manager)
    {
        $this->manager = $manager;
    }

    public function index()
    {
        $ordersList = $this->manager->getList(self::PER_PAGE);
        $pastDueOrders = $this->manager->getPastDueOrders();
        $currentOrders = $this->manager->getCurrentOrders();
        $newOrders = $this->manager->getNewOrders();
        $completedOrders = $this->manager->getCompletedOrders();
        return view('pages.order.orders', compact('ordersList', 'pastDueOrders', 'currentOrders', 'newOrders', 'completedOrders'));
    }

    public function edit(Order $order)
    {
        $statuses = OrderStatuses::getAllTranslations();
        $status = OrderStatuses::getKey($order->status);
        $partners = Partner::pluck('name', 'id');
        $cost = $this->manager->getCost($order);
        return view('pages.order.edit', compact('order', 'cost', 'partners', 'statuses', 'status'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'client_email' => 'required|email',
        ]);
        $this->manager->saveOrder($request->all());
        return redirect()->back()->with('status', trans('order.save-statuses.success'));
    }
}
