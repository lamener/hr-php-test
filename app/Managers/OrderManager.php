<?php

namespace App\Managers;

use App\Enums\OrderStatuses;
use App\Mail\OrderCompleted;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderManager
{
    public function getList($perPage)
    {
        $orders = Order::paginate($perPage);

        return $this->countOrdersCost($orders);
    }

    private function countOrdersCost($orders)
    {
        foreach ($orders as $order) {
            $order->total = $this->getCost($order);
        }

        return $orders;
    }

    public function getCost(Order $order)
    {
        $total = 0;

        foreach ($order->orderProducts as $product) {
            $total += $product->price * $product->quantity;
        }

        return $total;
    }

    public function saveOrder(array $data)
    {
        Order::where('id', $data['id'])
            ->update([
                'client_email' => $data['client_email'],
                'partner_id' => $data['partners'],
                'status' => OrderStatuses::constants()->get($data['statuses'])
            ]);

        if (OrderStatuses::constants()->get($data['statuses']) == OrderStatuses::COMPLETED) {
            $this->completedOrderSendMails($data['id']);
        }
    }

    private function completedOrderSendMails($order_id)
    {
        $order = Order::find($order_id);
        $cost = $this->getCost($order);
        $emails = collect([$order->client_email])
            ->merge($order->orderProducts->map(function ($product) {
                return $product->product->vendor->email;
            }));

        $emails->map(function ($email) use ($order, $cost) {
            Mail::to($email)->queue(new OrderCompleted($order, $cost));
        });
    }

    public function getPastDueOrders()
    {
        $orders = Order::whereStatus(OrderStatuses::CONFIRM)
            ->where('delivery_dt', '<', now())
            ->orderBy('delivery_dt', 'DESC')
            ->limit(50)
            ->get();

        return $this->countOrdersCost($orders);
    }

    public function getCurrentOrders()
    {
        $now = Carbon::now();
        $timeEnd = Carbon::now()->addHours(24);
        $orders = Order::whereStatus(OrderStatuses::CONFIRM)
            ->whereBetween('delivery_dt', [$now, $timeEnd])
            ->orderBy('delivery_dt', 'ASC')
            ->get();

        return $this->countOrdersCost($orders);
    }

    public function getNewOrders()
    {
        $orders = Order::whereStatus(OrderStatuses::NEW)
            ->where('delivery_dt', '>', now())
            ->orderBy('delivery_dt', 'ASC')
            ->limit(50)
            ->get();

        return $this->countOrdersCost($orders);
    }

    public function getCompletedOrders()
    {
        $orders = Order::whereStatus(OrderStatuses::COMPLETED)
            ->whereDate('delivery_dt', Carbon::today())
            ->orderBy('delivery_dt', 'ASC')
            ->limit(50)
            ->get();

        return $this->countOrdersCost($orders);
    }
}