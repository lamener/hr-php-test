<?php

namespace App\Enums;

class OrderStatuses extends AbstractEnum implements EnumInterface
{
    const NEW = 0; // новый
    const CONFIRM = 10; // подтвержден
    const COMPLETED = 20; // завершен

    public static function getTranslate($key)
    {
        $statuses = [
            self::NEW => trans('order.statuses.new'),
            self::CONFIRM => trans('order.statuses.confirm'),
            self::COMPLETED => trans('order.statuses.completed')
        ];
        return $statuses[$key];
    }
}