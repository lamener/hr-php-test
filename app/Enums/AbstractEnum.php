<?php

namespace App\Enums;

use ReflectionClass;

abstract class AbstractEnum
{
    public function __construct()
    {
    }

    public static function constants()
    {
        $reflector = new ReflectionClass(get_called_class());

        return collect($reflector->getConstants());
    }

    public static function getKey($value)
    {
        return self::constants()->search($value);
    }

    public static function getAllTranslations()
    {
        $class = get_called_class();
        return self::constants()->map(function ($item) use ($class) {
            return $class::getTranslate($item);
        });
    }


}