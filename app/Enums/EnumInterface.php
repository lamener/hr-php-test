<?php

namespace App\Enums;

interface EnumInterface
{
    public static function getTranslate($key);
}