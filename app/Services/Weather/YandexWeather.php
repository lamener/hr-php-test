<?php

namespace App\Services\Weather;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class YandexWeather implements WeatherInterface
{
    const MINUTES = 30;

    public function get($latitude, $longitude)
    {
        // кеширование добавлено так так по апи есть все 50 запросов в сутки.
        if (Cache::has('weather_json')) {
            return Cache::get('weather_json');
        }

        $client = new Client([
            'base_uri' => 'https://api.weather.yandex.ru/v1/',
            'timeout' => 2.0,
        ]);
        try {
            $response = $client->request('GET', 'informers', [
                'headers' => ['X-Yandex-API-Key' => 'c50581e2-6c75-4c4e-bf7a-e26e8a9199d6'],
                'query' => [
                    'lat' => $latitude,
                    'lon' => $longitude,
                ]
            ]);
        } catch (GuzzleException $e) {
            return [
                'error' => 'Сервис временно не доступен.'
            ];
        }

        $content = json_decode($response->getBody(), true);

        Cache::put('weather_json', $content, self::MINUTES);

        return $content;
    }


}