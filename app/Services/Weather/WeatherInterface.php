<?php
namespace App\Services\Weather;

interface WeatherInterface
{
    public function get($latitude, $longitude);
}